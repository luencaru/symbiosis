__author__ = 'lucaru9'
from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^api/users/check/$', CheckUserRegisterAPI.as_view()),
    url(r'^api/register/$', CreateUserAPI.as_view()),
    url(r'^api/users/$', RetrieveUserAPI.as_view()),
    url(r'^api/users/photos/$', UpdateUserPhotoAPI.as_view()),
    url(r'^api/login/$', LoginAPIView.as_view(), name='login'),
    url(r'^api/login/mobile/(?P<backend>[^/]+)/$', PSAMobileLoginAPI.as_view(), name="psa-mobile-login"),
    url(r'^api/me/devices/fcm/$', RegisterFCMDeviceAPI.as_view(), name='register-fcm-device'),
    url(r'^api/me/devices/fcm/update/$', UpdateFCMDeviceAPI.as_view(), name='update-fcm-device'),
    url(r'^api/user/recovery/(?P<id>\d+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm,
        {"post_reset_redirect": reverse_lazy(
            'account:password_reset_complete')},
        name='password_reset_confirm'),
    url(r'^api/reset/done/$', password_reset_complete,
        name='password_reset_complete'),
    url(r'^api/user/recovery/$', RecoveryPasswordAPI.as_view()),
    # APis doctor
    url(r'^api/doctors/register/$', CreateDoctorAPI.as_view()),

]
