# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-12-27 18:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20161227_1126'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='ce',
            field=models.CharField(blank=True, max_length=12, null=True, verbose_name='Carnet de extranjería'),
        ),
        migrations.AddField(
            model_name='user',
            name='passport',
            field=models.CharField(blank=True, max_length=12, null=True, verbose_name='Pasaporte'),
        ),
        migrations.AlterField(
            model_name='user',
            name='dni',
            field=models.CharField(blank=True, max_length=8, null=True, verbose_name='DNI'),
        ),
    ]
