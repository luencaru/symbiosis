# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-26 04:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_auto_20170125_2304'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='university',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Universidad'),
        ),
    ]
