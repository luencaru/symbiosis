from django.contrib.auth import authenticate
from fcm_django.models import FCMDevice
from requests import HTTPError
from rest_framework import serializers
from social.exceptions import AuthCanceled
from .models import User

__author__ = 'lucaru9'


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'cellphone',
                  'dni', 'ce', 'passport', 'mode_user')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class RetrieveUserSerializer(serializers.ModelSerializer):
    facebook_uid = serializers.SerializerMethodField(read_only=True)
    google_uid = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'cellphone', 'photo',
                  'mode_user', 'dni', 'ce', 'passport', 'facebook_uid', 'google_uid')
        read_only_fields = ('id', 'email', 'mode_user', 'facebook_uid', 'google_id', 'photo')

    def get_facebook_uid(self, obj):
        return obj.social_auth.filter(provider='facebook').first().uid if obj.social_auth.filter(
            provider='facebook').count() > 0 else None

    def get_google_uid(self, obj):
        return obj.social_auth.filter(provider='google-oauth2').first().uid if obj.social_auth.filter(
            provider='google-oauth2').count() > 0 else None


class UpdateUserPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('photo',)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(error_messages={"blank": "Este campo es obligatorio"})
    password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        self.user_cache = authenticate(email=attrs["email"], password=attrs["password"])
        if not self.user_cache:
            raise serializers.ValidationError("Invalid login")
        return attrs

    def get_user(self):
        return self.user_cache


class PSALoginSerializer(serializers.Serializer):
    access_token = serializers.CharField(
        error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        request = self.context.get("request")
        self.user_cache = None
        try:
            self.user_cache = request.backend.do_auth(attrs.get("access_token"))
            return attrs
        except HTTPError:
            raise serializers.ValidationError("Invalid token")
        except AuthCanceled:
            raise serializers.ValidationError("Token expired")

    def get_user(self):
        return self.user_cache


class FCMDeviceSerializer(serializers.ModelSerializer):
    registration_id = serializers.CharField(required=True)
    device_id = serializers.CharField(required=True)

    def validate_registration_id(self, attr):
        user = getattr(self.context.get("request"), "user")
        try:
            if type(eval(attr)).__name__ == 'dict':
                return eval(attr).get('token')
            else:
                raise serializers.ValidationError("Registration id inválido")
        except SyntaxError:
            return attr

    class Meta:
        model = FCMDevice
        fields = ('id', 'user', 'device_id', 'registration_id', 'name', 'type')


class UpdateFCMDeviceSerializer(serializers.ModelSerializer):
    registration_id = serializers.CharField(required=True)
    device_id = serializers.CharField(required=True)

    def validate_device_id(self, attr):
        user = getattr(self.context.get("request"), "user")
        if not self.Meta.model.objects.filter(device_id=attr, user=user).exists():
            raise serializers.ValidationError("El usuario no ha registrado este dispositivo")
        return attr

    class Meta:
        model = FCMDevice
        fields = ('id', 'user', 'device_id', 'registration_id', 'name', 'type')
        read_only_fields = ('user', 'name', 'type', 'device_id')


class PasswordRecoverySerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        self.cached_user = User.objects.filter(email=value).first()
        if not self.cached_user:
            raise serializers.ValidationError("The email is not registered")
        return value


class CheckUserSerializer(serializers.Serializer):
    email = serializers.EmailField(error_messages={"blank": "Este campo es obligatorio"})


class CreateDoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'cellphone', 'university',
                  'dni', 'ce', 'passport', 'mode_user', 'cmp', 'experience', 'experience_minsa',
                  'experience_essalud', 'experience_ffaa', 'experience_priv', 'is_enabled')
        write_only_fields = ('password',)
        read_only_fields = ('id', 'is_enabled')

    def create(self, validated_data):
        validated_data['is_enabled'] = False
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user



