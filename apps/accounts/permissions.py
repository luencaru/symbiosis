from rest_framework.permissions import BasePermission

__author__ = 'lucaru9'


class IsDoctor(BasePermission):
    """
    Allows access only to authenticated users doctors.
    """

    def has_permission(self, request, view):
        return request.user.mode_user == 'p' and request.user.is_enabled


class IsSecretary(BasePermission):
    """
    Allows access only to authenticated users doctors.
    """

    def has_permission(self, request, view):
        return request.user.mode_user == 's' and request.user.is_enabled


class IsDoctorOrSecretary(BasePermission):
    """
    Allows access only to authenticated users doctors.
    """

    def has_permission(self, request, view):
        return (request.user.mode_user == 'p' or request.user.mode_user == 's') and request.user.is_enabled
