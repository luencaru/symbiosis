from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import deprecate_current_app
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, resolve_url
from django.utils.translation import ugettext as _
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from fcm_django.models import FCMDevice
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.generics import CreateAPIView, RetrieveAPIView, GenericAPIView, UpdateAPIView, \
    RetrieveUpdateAPIView, \
    get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from social.apps.django_app.utils import psa
from .models import User
from .tasks import recovery_password_mail
from .serializers import CreateUserSerializer, RetrieveUserSerializer, LoginSerializer, PSALoginSerializer, \
    FCMDeviceSerializer, UpdateFCMDeviceSerializer, UpdateUserPhotoSerializer, PasswordRecoverySerializer, \
    CheckUserSerializer, CreateDoctorSerializer


class CreateUserAPI(CreateAPIView):
    """ Registro de usuario
        gender=(M o F)
        mode_user = (u o p) u=usuario p=profesional
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)
    serializer_class = CreateUserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class RetrieveUserAPI(RetrieveUpdateAPIView):
    '''GET:Devuelve el usuario logueado
        PUT or PATCH: modifica el usuarion ('first_name','first_last_name','second_last_name','cellphone','gender','dni')
    '''
    serializer_class = RetrieveUserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        if self.request.user.mode_user == 'u':
            return self.request.user
        else:
            raise Http404("Comprobar que sea paciente")


class UpdateUserPhotoAPI(UpdateAPIView):
    serializer_class = UpdateUserPhotoSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class LoginAPIView(GenericAPIView):
    '''login api'''
    serializer_class = LoginSerializer
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.get_user())
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class MobileLoginAPI(GenericAPIView):
    serializer_class = LoginSerializer
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.get_user())
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class PSAMobileLoginAPI(MobileLoginAPI):
    '''Facebook Login'''
    serializer_class = PSALoginSerializer

    @method_decorator(psa('account:psa-mobile-login'))
    def dispatch(self, request, *args, **kwargs):
        return super(PSAMobileLoginAPI, self).dispatch(request, *args, **kwargs)


# class GoogleMobileLoginAPI(MobileLoginAPI):
#     '''Facebook Login'''
#     serializer_class = PSALoginSerializer
#
#     @method_decorator(psa('account:google-mobile-login'))
#     def dispatch(self, request, *args, **kwargs):
#         return super(GoogleMobileLoginAPI, self).dispatch(request, *args, **kwargs)

class RegisterFCMDeviceAPI(CreateAPIView):
    '''Registrar Dispositivo especificanco si es android o ios en el campo type'''
    serializer_class = FCMDeviceSerializer
    pagination_class = None
    permission_classes = IsAuthenticated,

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data
        registration_id = serializer.validated_data.get("registration_id")
        device = FCMDevice.objects.filter(registration_id=registration_id).first()
        if device:
            device.user = self.request.user
            device.name = vd.get("name")
            device.device_id = vd.get("device_id")
            device.save()
            return Response(FCMDeviceSerializer(device).data, status=status.HTTP_201_CREATED)
        else:
            serializer.save(user=self.request.user)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UpdateFCMDeviceAPI(UpdateAPIView):
    '''
        Enviar device_id y registration_id(token de android)
    '''
    permission_classes = IsAuthenticated,
    serializer_class = UpdateFCMDeviceSerializer

    def get_object(self):
        return get_object_or_404(FCMDevice.objects.filter(user=self.request.user),
                                 device_id=self.request.data.get('device_id'))


class RecoveryPasswordAPI(GenericAPIView):
    '''Recuperar contraseña'''
    serializer_class = PasswordRecoverySerializer
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data
        recovery_password_mail(vd.get("email"), request)
        return Response({"detail": "OK"}, status=status.HTTP_200_OK)


@sensitive_post_parameters()
@never_cache
@deprecate_current_app
def password_reset_confirm(request, id=None, token=None,
                           template_name='registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm,
                           post_reset_redirect=None,
                           extra_context=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert id is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        user = User.objects.get(pk=id)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('Enter new password')
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = _('Password reset unsuccessful')
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


@deprecate_current_app
def password_reset_complete(request,
                            template_name='registration/password_reset_complete.html',
                            extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL),
        'title': _('Password reset complete'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


class CheckUserRegisterAPI(GenericAPIView):
    '''
    APi para verificar el correo ( 200 True(puede registrar), 303 False(correo registrado) )
    '''
    serializer_class = CheckUserSerializer
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data.get('email')
        if User.objects.filter(email=email).exists():
            return Response({'check': False}, status=status.HTTP_303_SEE_OTHER)
        else:
            return Response({'check': True}, status=status.HTTP_200_OK)


class CreateDoctorAPI(CreateAPIView):
    """ Registro de usuario
        mode_user = (u o p) u=usuario p=profesional
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)
    serializer_class = CreateDoctorSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key}, status=status.HTTP_200_OK)



