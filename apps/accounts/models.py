# encoding= utf-8
from enum import Enum
from django.contrib.auth.models import PermissionsMixin, BaseUserManager, \
    AbstractBaseUser
from django.core.validators import MinValueValidator
from django.core.validators import MaxValueValidator
from django.db import models

MODE_USER_CHOICES = (('u', 'Paciente'), ('p', 'Profesional'), ('s', 'Secretaria'))


class Types(Enum):
    qt = "Quintos"
    cf = "Cuota Fija"


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser,
                     **extra_fields):
        user = self.model(email=email, is_active=True,
                          is_staff=is_staff, is_superuser=is_superuser,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, username, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    cellphone = models.CharField(max_length=9, blank=True, null=True)
    photo = models.ImageField(upload_to='user', blank=True, null=True)
    dni = models.CharField(max_length=8, null=True, blank=True, verbose_name='DNI')
    ce = models.CharField(max_length=12, null=True, blank=True, verbose_name='Carnet de extranjería')
    passport = models.CharField(max_length=12, null=True, blank=True, verbose_name='Pasaporte')
    mode_user = models.CharField(max_length=1, default='u', choices=MODE_USER_CHOICES, verbose_name='tipo de usuario')
    cmp = models.CharField(max_length=10, null=True, blank=True, verbose_name='Número de colegiatura')
    experience = models.PositiveIntegerField(null=True, blank=True, verbose_name='Años de Experiencia')
    experience_minsa = models.BooleanField(default=False, verbose_name='Experiencia en MINSA')
    experience_essalud = models.BooleanField(default=False, verbose_name='Experiencia en ESSALUD')
    experience_ffaa = models.BooleanField(default=False, verbose_name='Experiencia en FFAA')
    experience_priv = models.BooleanField(default=False, verbose_name='Experiencia en Institutos Privados')
    university = models.CharField(max_length=200, null=True, blank=True, verbose_name='Universidad')
    payment_type = models.CharField(max_length=2, choices=[(item.name, item.value) for item in Types],
                                    verbose_name='Tipo de pago seleccionado', null=True, blank=True)
    is_enabled = models.BooleanField(default=True, verbose_name='Habilitar usuario')
    # date_activation = models.DateField(null=True, blank=True, verbose_name='Fecha de activación')
    doctor = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='secretaries', null=True, blank=True)
    objects = UserManager()
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'

    def get_user_face(self):
        if self.social_auth.count() > 0:
            return True
        else:
            return False

    def get_full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        return '{0}'.format(self.first_name)

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"
