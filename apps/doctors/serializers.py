from rest_framework import serializers
from ..accounts.models import User
from .models import District, City, DoctorOffice, Payment, Speciality, Schedule, SpecialSchedule

__author__ = 'lucaru9'


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ('id', 'name')


class CitySerializer(serializers.ModelSerializer):
    districts = DistrictSerializer(many=True, read_only=True)

    class Meta:
        model = City
        fields = ('id', 'name', 'districts')


class CityInDistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name')


class DetailDistrictSerializer(serializers.ModelSerializer):
    city = CityInDistrictSerializer(read_only=True)

    class Meta:
        model = District
        fields = ('id', 'name', 'city')


class CreateDoctorOfficeSerializer(serializers.ModelSerializer):
    district_detail = DetailDistrictSerializer(read_only=True, source='district')

    class Meta:
        model = DoctorOffice
        fields = ('id', 'created_at', 'name', 'phone', 'postal_code', 'address', 'longitude', 'latitude', 'is_enabled',
                  'detail', 'other_phone', 'district', 'district_detail', 'low_demand_price', 'high_demand_price')
        read_only_fields = ('doctor',)
        extra_kwargs = {'district': {'write_only': True}}


class RetrieveUpdateDoctorOfficeSerializer(serializers.ModelSerializer):
    district_detail = DetailDistrictSerializer(read_only=True, source='district')

    class Meta:
        model = DoctorOffice
        fields = ('id', 'created_at', 'name', 'phone', 'postal_code', 'address', 'longitude', 'latitude', 'is_enabled',
                  'detail', 'other_phone', 'district', 'district_detail', 'low_demand_price', 'high_demand_price')
        extra_kwargs = {'district': {'write_only': True}}


class ChangePaymentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'payment_type')


class ListSpecialitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Speciality
        fields = ('id', 'name')


class RetrieveDoctorSerializer(serializers.ModelSerializer):
    specialities = ListSpecialitiesSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ('id', 'is_enabled', 'email', 'first_name', 'last_name', 'photo', 'cellphone', 'university', 'dni',
                  'ce', 'passport', 'mode_user', 'cmp', 'experience', 'experience_minsa', 'experience_essalud',
                  'experience_ffaa', 'experience_priv', 'specialities')


class RetrieveUpdateDoctorSerializer(serializers.ModelSerializer):
    specialities = ListSpecialitiesSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ('id', 'is_enabled', 'email', 'first_name', 'last_name', 'photo', 'cellphone', 'university', 'dni',
                  'ce', 'passport', 'mode_user', 'cmp', 'experience', 'experience_minsa', 'experience_essalud',
                  'experience_ffaa', 'experience_priv', 'specialities')
        read_only_fields = ('photo',)


class CreateListSecretarySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'password', 'doctor', 'is_enabled', 'cellphone')
        extra_kwargs = {'doctor': {'write_only': True}, 'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class RetrieveUpdateDestroySecretarySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'is_enabled', 'cellphone')
        read_only_fields = ('email',)


class RegisterSpecialScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpecialSchedule
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at')


class RegisterScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at')
