from django.db.models import Prefetch
import datetime
from django.http import Http404
from rest_framework import filters
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, GenericAPIView, RetrieveAPIView, \
    RetrieveUpdateAPIView, ListCreateAPIView, get_object_or_404, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from ..accounts.models import Types
from ..accounts.permissions import IsDoctor, IsDoctorOrSecretary
from .models import City, District, Payment, Amount, Speciality
from .serializers import CitySerializer, CreateDoctorOfficeSerializer, ChangePaymentTypeSerializer, \
    ListSpecialitiesSerializer, RetrieveDoctorSerializer, RetrieveUpdateDoctorSerializer, \
    RetrieveUpdateDoctorOfficeSerializer, CreateListSecretarySerializer, RetrieveUpdateDestroySecretarySerializer, \
    RegisterScheduleSerializer, RegisterSpecialScheduleSerializer
from rest_framework import status


class ListCitiesAPI(ListAPIView):
    '''
    Lista las ciudades con sus distritos api/cities/?search='' para buscar ciudad por nombre
    '''
    serializer_class = CitySerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)
    queryset = City.objects.filter(is_enabled=True).prefetch_related(
        Prefetch('districts', queryset=District.objects.filter(is_enabled=True)))


class CreateDoctorOfficeAPI(ListCreateAPIView):
    '''
    Crea Consultorios para un medico no es necesario enviar field doctor
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CreateDoctorOfficeSerializer

    def perform_create(self, serializer):
        serializer.save(doctor=self.request.user)

    def get_queryset(self):
        return self.request.user.offices.all()


class RetrieveUpdateDestroyOfficeAPI(RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = RetrieveUpdateDoctorOfficeSerializer

    def get_object(self):
        return get_object_or_404(self.request.user.offices.all(), pk=self.kwargs.get('pk'))


class ChangePaymentTypeAPI(UpdateAPIView):
    '''
    Cambia el metodo de pago en general el cambio se reazliza en el siguiente mes
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePaymentTypeSerializer

    def get_object(self):
        if self.request.user.mode_user == 'p':
            return self.request.user
        else:
            return Response({'detail': 'Comprobar doctor'}, status=status.HTTP_404_NOT_FOUND)


class SelectPaymentTypeAPI(GenericAPIView):
    '''
    Selecciona el metodo de pago despues del registro en el body "payment_type" qt = "Quintos"
    cf = "Cuota Fija"
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        payment_type = request.data.get('payment_type')
        band = False if request.user.payments.filter(is_free=True).count() >= 2 else True
        if payment_type == Types.cf.name:
            date = datetime.date.today()
            Payment.objects.create(month=date.month, year=date.year, payment_type=Types.cf.name,
                                   amount=Amount.objects.first().amount, doctor=request.user, is_free=band)
            request.user.payment_type = Types.cf.name
            request.user.save()
            return Response({'detail': 'Método de pago seleccionado'}, status=status.HTTP_201_CREATED)
        elif payment_type == Types.qt.name:
            date = datetime.date.today()
            Payment.objects.create(month=date.month, year=date.year, payment_type=Types.qt.name, doctor=request.user,
                                   is_free=band)
            request.user.payment_type = Types.qt.name
            request.user.save()
            return Response({'detail': 'Método de pago seleccionado'}, status=status.HTTP_201_CREATED)
        else:
            return Response({'detail': 'Método de pago incorrecto'}, status=status.HTTP_404_NOT_FOUND)


class ListSpecialitiesAPI(ListAPIView):
    '''
    Lista especialidades api/specialities/?search='' para buscar por nombre
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ListSpecialitiesSerializer
    queryset = Speciality.objects.filter(is_enabled=True)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)


class AddSpecialitiesAPI(GenericAPIView):
    '''
    Agrega especialidades al doctor POST fiels specialities = [id,id]
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        specialities = Speciality.objects.filter(is_enabled=True, id__in=eval(self.request.data.get('specialities')))
        for specialitie in specialities:
            specialitie.doctors.add(self.request.user)

        return Response({'detail': 'Especialidades agregadas'}, status=status.HTTP_200_OK)


class RetrieveUpdateDoctorAPI(RetrieveUpdateAPIView):
    '''GET:Devuelve el doctor logueado
        PUT or PATCH: modifica al doctor ( 'email', 'password', 'first_name', 'last_name', 'cellphone', 'photo',
                  'dni', 'ce', 'passport', 'mode_user', 'cmp', 'experience_from', 'experience_minsa',
                  'experience_essalud', 'experience_ffaa', 'experience_priv')
    '''
    serializer_class = RetrieveUpdateDoctorSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        if self.request.user.mode_user == 'p':
            return self.request.user
        else:
            raise Http404("Comprobar que sea doctor")


class ListCreateSecretariesAPI(ListCreateAPIView):
    '''
    POST: Crea (No es necesario enviar el campo doctor), GET:lista secretarias
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctor)
    serializer_class = CreateListSecretarySerializer

    def perform_create(self, serializer):
        serializer.save(doctor=self.request.user, mode_user='s')

    def get_queryset(self):
        return self.request.user.secretaries.all()


class RetrieveUpdateDestroySecretariesAPI(RetrieveUpdateDestroyAPIView):
    '''
    GET: Detalle, PUT-PATCH: Editar (first_name, last_name, is_enabled), DELETE: Elimninar
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctor)
    serializer_class = RetrieveUpdateDestroySecretarySerializer

    def get_object(self):
        return get_object_or_404(self.request.user.secretaries.all(), id=self.kwargs.get('pk'))


class RegisterScheduleAPI(CreateAPIView):
    '''
    Registra el horario de un doctor por dia y consultorio
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctorOrSecretary)
    serializer_class = RegisterScheduleSerializer


class ListScheduleForDoctorOfficeAPI(ListAPIView):
    '''
    Lista los horarios por consultorio
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctorOrSecretary)
    serializer_class = RegisterScheduleSerializer

    def get_queryset(self):
        doctor_office = get_object_or_404(self.request.user.offices.all(), pk=self.kwargs.get('pk'))
        return doctor_office.schedules.all()


class RegisterSpecialScheduleAPI(CreateAPIView):
    '''
    Registra el horario especial de un doctor por dia y consultorio
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctorOrSecretary)
    serializer_class = RegisterSpecialScheduleSerializer


class ListSpecialScheduleForDoctorOfficeAPI(ListAPIView):
    '''
    Lista los horarios especiales por consultorio apartir de hoy
    '''
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctorOrSecretary)
    serializer_class = RegisterSpecialScheduleSerializer

    def get_queryset(self):
        doctor_office = get_object_or_404(self.request.user.offices.all(), pk=self.kwargs.get('pk'))
        return doctor_office.special_schedules.filter(date__gte=datetime.datetime.today().date())
