__author__ = 'lucaru9'

from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^api/doctor/$', RetrieveUpdateDoctorAPI.as_view()),
    url(r'^api/select-payment-type/$', SelectPaymentTypeAPI.as_view()),
    url(r'^api/change-payment-type/$', ChangePaymentTypeAPI.as_view()),
    url(r'^api/cities/$', ListCitiesAPI.as_view()),
    url(r'^api/specialities/$', ListSpecialitiesAPI.as_view()),
    url(r'^api/add-specialities/$', AddSpecialitiesAPI.as_view()),
    url(r'^api/doctoroffices/$', CreateDoctorOfficeAPI.as_view()),
    url(r'^api/doctoroffices/(?P<pk>\d+)/$', RetrieveUpdateDestroyOfficeAPI.as_view()),
    url(r'^api/secretaries/$', ListCreateSecretariesAPI.as_view()),
    url(r'^api/secretaries/(?P<pk>\d+)/$', RetrieveUpdateDestroySecretariesAPI.as_view()),
    url(r'^api/doctoroffices/register-schedules/$', RegisterScheduleAPI.as_view()),
    url(r'^api/doctoroffices/(?P<pk>\d+)/schedules/$', ListScheduleForDoctorOfficeAPI.as_view()),
    url(r'^api/doctoroffices/register-special-schedules/$', RegisterSpecialScheduleAPI.as_view()),
    url(r'^api/doctoroffices/(?P<pk>\d+)/special-schedules/$', ListSpecialScheduleForDoctorOfficeAPI.as_view()),
]
