from django.contrib import admin

# Register your models here.
from .models import District, City, DoctorOffice, Payment, Amount, Speciality

admin.site.register(District)
admin.site.register(City)
admin.site.register(DoctorOffice)
admin.site.register(Payment)
admin.site.register(Amount)
admin.site.register(Speciality)
