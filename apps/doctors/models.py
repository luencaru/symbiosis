from enum import Enum
from django.db import models


# Create your models here.
from apps.accounts.models import User, Types


class Amount(models.Model):
    amount = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('amount',)
        verbose_name = 'Monto Mensual'
        verbose_name_plural = 'Monto Mensual'

    def __str__(self):
        return u'{}'.format(self.name)


class City(models.Model):
    name = models.CharField(max_length=30, verbose_name='nombre', unique=True)
    is_enabled = models.BooleanField(default=True, verbose_name='Ciudad habilitada')

    class Meta:
        ordering = ('name',)
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'

    def __str__(self):
        return u'{}'.format(self.name)


class District(models.Model):
    name = models.CharField(max_length=30, verbose_name='nombre', unique=True)
    city = models.ForeignKey(City, verbose_name='ciudad', related_name='districts')
    is_enabled = models.BooleanField(default=True, verbose_name='Distrito habilitado')

    class Meta:
        ordering = ('name',)
        verbose_name = 'Distrito'
        verbose_name_plural = 'Distritos'

    def __str__(self):
        return u'{} - {}'.format(self.city, self.name)


class DoctorOffice(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, verbose_name='Nombre del consultorio')
    phone = models.CharField(max_length=12, verbose_name='Teléfono del consultorio')
    other_phone = models.CharField(max_length=12, verbose_name='Teléfono 2 del consultorio', null=True, blank=True)
    postal_code = models.CharField(max_length=5, verbose_name='Código Postal', blank=True)
    address = models.CharField(max_length=500, verbose_name='Dirección')
    longitude = models.DecimalField(max_digits=20, decimal_places=17, verbose_name='Longitud', null=True, blank=True)
    latitude = models.DecimalField(max_digits=20, decimal_places=17, verbose_name='Latitud', null=True, blank=True)
    detail = models.TextField(null=True, blank=True, verbose_name='Detalle de los servicios')
    is_enabled = models.BooleanField(default=True, verbose_name='Consultorio habilitado')
    doctor = models.ForeignKey(User, related_name='offices', verbose_name='Doctor')
    district = models.ForeignKey(District, related_name='offices', verbose_name='Distrito')
    low_demand_price = models.PositiveIntegerField(verbose_name='Precio demanda baja', null=True, blank=True)
    high_demand_price = models.PositiveIntegerField(verbose_name='Precio demanda alta', null=True, blank=True)

    class Meta:
        verbose_name_plural = "Consultorios"
        verbose_name = "Consultorio"
        ordering = ['name']

    def __str__(self):
        return u'{} - {} - {}'.format(self.doctor.get_full_name(), self.name, self.district.name)


class Payment(models.Model):
    class States(Enum):
        c = "Cancelado"
        p = "Pendiente"
        v = "Vencido"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    month = models.CharField(max_length=11, verbose_name='Mes')
    year = models.CharField(max_length=4, verbose_name='Año')
    payment_type = models.CharField(max_length=2, choices=[(item.name, item.value) for item in Types],
                                    verbose_name='Tipo de pago')
    amount = models.PositiveIntegerField(verbose_name='Monto del pago', null=True, blank=True)
    is_free = models.BooleanField(default=False, verbose_name='El mes es gratis?')
    is_paid = models.BooleanField(default=False, verbose_name='El mes está pagado?')
    paid_day = models.DateField(blank=True, null=True, verbose_name='Día en que realizó el pago')
    payment_state = models.CharField(max_length=1, choices=[(item.name, item.value) for item in States],
                                     verbose_name='Estado del pago', default=States.p.name)
    ranking = models.PositiveIntegerField(null=True, blank=True, verbose_name='Ranking del tipo Quinto')
    number_appointments = models.PositiveIntegerField(null=True, blank=True, verbose_name='Cantidad de citas')
    doctor = models.ForeignKey(User, related_name='payments', verbose_name='Doctor')

    class Meta:
        verbose_name_plural = "Pagos Mensuales"
        verbose_name = "Pago mensual"
        ordering = ['year', 'month']

    def __str__(self):
        return u'{} - {} - {}'.format(self.doctor.get_full_name(), self.year, self.month)


class Speciality(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, verbose_name='Nombre de la especialidad')
    is_enabled = models.BooleanField(default=True, verbose_name='Especialidad habilitada')
    doctors = models.ManyToManyField(User, related_name='specialities', blank=True)

    class Meta:
        verbose_name_plural = "Especialidades Médicas"
        verbose_name = "Especialidad Médica"
        ordering = ['name']

    def __str__(self):
        return u'{}'.format(self.name)


class Schedule(models.Model):
    class Days(Enum):
        mon = "Lunes"
        tue = "Martes"
        wed = "Miércoles"
        thu = "Jueves"
        fri = "Viernes"
        sat = "Sábado"
        sun = "Domingo"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    day = models.CharField(max_length=3, verbose_name='Día de semana',
                           choices=[(item.name, item.value) for item in Days])
    is_enabled = models.BooleanField(default=True, verbose_name='Habilitar dia')
    doctor_office = models.ForeignKey(DoctorOffice, related_name='schedules', verbose_name='Consultorio')
    _800_830 = models.BooleanField(default=False, verbose_name='8:00 a 8:30')
    _830_900 = models.BooleanField(default=False, verbose_name='8:30 a 9:00')
    _900_830 = models.BooleanField(default=False, verbose_name='9:00 a 9:30')
    _930_1000 = models.BooleanField(default=False, verbose_name='9:30 a 10:00')
    _1000_1030 = models.BooleanField(default=False, verbose_name='10:00 a 10:30')
    _1030_1100 = models.BooleanField(default=False, verbose_name='10:30 a 11:00')
    _1100_1130 = models.BooleanField(default=False, verbose_name='11:00 a 11:30')
    _1130_1200 = models.BooleanField(default=False, verbose_name='11:30 a 12:00')
    _1200_1230 = models.BooleanField(default=False, verbose_name='12:00 a 12:30')
    _1230_1300 = models.BooleanField(default=False, verbose_name='12:30 a 13:00')
    _1300_1330 = models.BooleanField(default=False, verbose_name='13:00 a 13:30')
    _1330_1400 = models.BooleanField(default=False, verbose_name='13:30 a 14:00')
    _1400_1430 = models.BooleanField(default=False, verbose_name='14:00 a 14:30')
    _1430_1500 = models.BooleanField(default=False, verbose_name='14:30 a 15:00')
    _1500_1530 = models.BooleanField(default=False, verbose_name='15:00 a 15:30')
    _1530_1600 = models.BooleanField(default=False, verbose_name='15:30 a 16:00')
    _1600_1630 = models.BooleanField(default=False, verbose_name='16:00 a 16:30')
    _1630_1700 = models.BooleanField(default=False, verbose_name='16:30 a 17:00')
    _1700_1730 = models.BooleanField(default=False, verbose_name='17:00 a 17:30')
    _1730_1800 = models.BooleanField(default=False, verbose_name='17:30 a 18:00')
    _1800_1830 = models.BooleanField(default=False, verbose_name='18:00 a 18:30')
    _1830_1900 = models.BooleanField(default=False, verbose_name='18:30 a 19:00')
    _1900_1930 = models.BooleanField(default=False, verbose_name='19:00 a 19:30')
    _1930_2000 = models.BooleanField(default=False, verbose_name='19:30 a 20:00')
    _2000_2030 = models.BooleanField(default=False, verbose_name='20:00 a 20:30')
    _2030_2100 = models.BooleanField(default=False, verbose_name='20:30 a 21:00')

    class Meta:
        verbose_name_plural = "Horarios semanales"
        verbose_name = "Horario semanal"
        ordering = ['created_at']
        unique_together = ('day', 'doctor_office')

    def __str__(self):
        return u'{} {} {}'.format(self.doctor_office.doctor.get_full_name(), self.doctor_office.name, self.day)


class SpecialSchedule(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    date = models.DateField(verbose_name='Fecha')
    all_closed = models.BooleanField(default=False, verbose_name='Cerrar todo el dia')
    doctor_office = models.ForeignKey(DoctorOffice, related_name='special_schedules', verbose_name='Consultorio')
    _800_830 = models.BooleanField(default=False, verbose_name='8:00 a 8:30')
    _830_900 = models.BooleanField(default=False, verbose_name='8:30 a 9:00')
    _900_830 = models.BooleanField(default=False, verbose_name='9:00 a 9:30')
    _930_1000 = models.BooleanField(default=False, verbose_name='9:30 a 10:00')
    _1000_1030 = models.BooleanField(default=False, verbose_name='10:00 a 10:30')
    _1030_1100 = models.BooleanField(default=False, verbose_name='10:30 a 11:00')
    _1100_1130 = models.BooleanField(default=False, verbose_name='11:00 a 11:30')
    _1130_1200 = models.BooleanField(default=False, verbose_name='11:30 a 12:00')
    _1200_1230 = models.BooleanField(default=False, verbose_name='12:00 a 12:30')
    _1230_1300 = models.BooleanField(default=False, verbose_name='12:30 a 13:00')
    _1300_1330 = models.BooleanField(default=False, verbose_name='13:00 a 13:30')
    _1330_1400 = models.BooleanField(default=False, verbose_name='13:30 a 14:00')
    _1400_1430 = models.BooleanField(default=False, verbose_name='14:00 a 14:30')
    _1430_1500 = models.BooleanField(default=False, verbose_name='14:30 a 15:00')
    _1500_1530 = models.BooleanField(default=False, verbose_name='15:00 a 15:30')
    _1530_1600 = models.BooleanField(default=False, verbose_name='15:30 a 16:00')
    _1600_1630 = models.BooleanField(default=False, verbose_name='16:00 a 16:30')
    _1630_1700 = models.BooleanField(default=False, verbose_name='16:30 a 17:00')
    _1700_1730 = models.BooleanField(default=False, verbose_name='17:00 a 17:30')
    _1730_1800 = models.BooleanField(default=False, verbose_name='17:30 a 18:00')
    _1800_1830 = models.BooleanField(default=False, verbose_name='18:00 a 18:30')
    _1830_1900 = models.BooleanField(default=False, verbose_name='18:30 a 19:00')
    _1900_1930 = models.BooleanField(default=False, verbose_name='19:00 a 19:30')
    _1930_2000 = models.BooleanField(default=False, verbose_name='19:30 a 20:00')
    _2000_2030 = models.BooleanField(default=False, verbose_name='20:00 a 20:30')
    _2030_2100 = models.BooleanField(default=False, verbose_name='20:30 a 21:00')

    class Meta:
        verbose_name_plural = "Horarios especiales"
        verbose_name = "Horario especial"
        ordering = ['created_at']
        unique_together = ('date', 'doctor_office')

    def __str__(self):
        return u'{} {} {}'.format(self.doctor_office.doctor.get_full_name(), self.doctor_office.name, self.date)
