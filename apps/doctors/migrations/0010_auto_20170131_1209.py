# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-31 17:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('doctors', '0009_auto_20170130_2337'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('day', models.CharField(choices=[('mon', 'Lunes'), ('tue', 'Martes'), ('wed', 'Miércoles'), ('thu', 'Jueves'), ('fri', 'Viernes'), ('sat', 'Sábado'), ('sun', 'Domingo')], max_length=3, verbose_name='Día de semana')),
                ('is_enabled', models.BooleanField(default=True, verbose_name='Habilitar dia')),
                ('_800_830', models.BooleanField(default=False, verbose_name='8:00 a 8:30')),
                ('_830_900', models.BooleanField(default=False, verbose_name='8:30 a 9:00')),
                ('_900_830', models.BooleanField(default=False, verbose_name='9:00 a 9:30')),
                ('_930_1000', models.BooleanField(default=False, verbose_name='9:30 a 10:00')),
                ('_1000_1030', models.BooleanField(default=False, verbose_name='10:00 a 10:30')),
                ('_1030_1100', models.BooleanField(default=False, verbose_name='10:30 a 11:00')),
                ('_1100_1130', models.BooleanField(default=False, verbose_name='11:00 a 11:30')),
                ('_1130_1200', models.BooleanField(default=False, verbose_name='11:30 a 12:00')),
                ('_1200_1230', models.BooleanField(default=False, verbose_name='12:00 a 12:30')),
                ('_1230_1300', models.BooleanField(default=False, verbose_name='12:30 a 13:00')),
                ('_1300_1330', models.BooleanField(default=False, verbose_name='13:00 a 13:30')),
                ('_1330_1400', models.BooleanField(default=False, verbose_name='13:30 a 14:00')),
                ('_1400_1430', models.BooleanField(default=False, verbose_name='14:00 a 14:30')),
                ('_1430_1500', models.BooleanField(default=False, verbose_name='14:30 a 15:00')),
                ('_1500_1530', models.BooleanField(default=False, verbose_name='15:00 a 15:30')),
                ('_1530_1600', models.BooleanField(default=False, verbose_name='15:30 a 16:00')),
                ('_1600_1630', models.BooleanField(default=False, verbose_name='16:00 a 16:30')),
                ('_1630_1700', models.BooleanField(default=False, verbose_name='16:30 a 17:00')),
                ('_1700_1730', models.BooleanField(default=False, verbose_name='17:00 a 17:30')),
                ('_1730_1800', models.BooleanField(default=False, verbose_name='17:30 a 18:00')),
                ('_1800_1830', models.BooleanField(default=False, verbose_name='18:00 a 18:30')),
                ('_1830_1900', models.BooleanField(default=False, verbose_name='18:30 a 19:00')),
                ('_1900_1930', models.BooleanField(default=False, verbose_name='19:00 a 19:30')),
                ('_1930_2000', models.BooleanField(default=False, verbose_name='19:30 a 20:00')),
                ('_2000_2030', models.BooleanField(default=False, verbose_name='20:00 a 20:30')),
                ('_2030_2100', models.BooleanField(default=False, verbose_name='20:30 a 21:00')),
                ('doctor_office', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='schedules', to='doctors.DoctorOffice', verbose_name='Consultorio')),
            ],
            options={
                'verbose_name': 'Horario semanal',
                'verbose_name_plural': 'Horarios semanales',
                'ordering': ['created_at'],
            },
        ),
        migrations.AlterUniqueTogether(
            name='schedule',
            unique_together=set([('day', 'doctor_office')]),
        ),
    ]
