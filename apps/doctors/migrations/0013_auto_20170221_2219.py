# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-02-22 03:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('doctors', '0012_auto_20170206_2259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialschedule',
            name='doctor_office',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='special_schedules', to='doctors.DoctorOffice', verbose_name='Consultorio'),
        ),
    ]
