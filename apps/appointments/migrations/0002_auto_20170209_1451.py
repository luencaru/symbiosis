# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-02-09 19:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appointments', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='patient_dni',
            field=models.CharField(blank=True, max_length=8, null=True, verbose_name='DNI Paciente'),
        ),
        migrations.AddField(
            model_name='appointment',
            name='patient_fullname',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='NOMBRE Paciente'),
        ),
    ]
