from rest_framework import serializers
from .models import Appointment

__author__ = 'lucaru9'


class RegisterAppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ('id', 'code', 'date', 'time_start', 'time_end', 'doctor_office', 'state', 'is_rated',
                  'patient_fullname', 'patient_dni', 'worry', 'knowledge', 'opinion', 'is_valid')
        read_only_fields = ('id', 'code', 'state', 'is_rated', 'worry', 'knowledge', 'opinion', 'doctor_office',
                            'is_valid')
