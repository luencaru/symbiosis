from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView
from django.views.generic.edit import ModelFormMixin
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import CreateAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from ..accounts.permissions import IsDoctorOrSecretary
from .models import Appointment
from .forms import FormCheckAppointment, FormQualifyAppointment
from ..doctors.models import DoctorOffice
from .serializers import RegisterAppointmentSerializer


class RegisterAppointmentAPI(ListCreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDoctorOrSecretary)
    serializer_class = RegisterAppointmentSerializer

    def get_queryset(self):
        doctor_office = get_object_or_404(DoctorOffice.objects.all(), pk=self.kwargs.get('pk'))
        return doctor_office.appointments.all()

    def perform_create(self, serializer):
        doctor_office = get_object_or_404(DoctorOffice.objects.all(), pk=self.kwargs.get('pk'))
        serializer.save(doctor_office=doctor_office, type_register=Appointment.TypesRegister.rm.name)


class CheckAppointmentView(FormView):
    form_class = FormCheckAppointment
    template_name = 'Appointments/check-appointment.html'

    def form_valid(self, form):
        appointment = get_object_or_404(Appointment.objects.all(), code=form.cleaned_data.get('code'))
        appointment.is_valid = True
        appointment.save()
        url = reverse('appointments:qualify', kwargs={'code': appointment.code})
        return HttpResponseRedirect(url)


class QualifyAppointmentView(FormView):
    form_class = FormQualifyAppointment
    template_name = 'Appointments/qualify.html'
    success_url = 'appointments:validate'

    def form_valid(self, form):
        appointment = get_object_or_404(Appointment.objects.all(), code=self.kwargs.get('code'))
        appointment.worry = form.cleaned_data.get('worry')
        appointment.knowledge = form.cleaned_data.get('knowledge')
        appointment.opinion = form.cleaned_data.get('opinion')
        appointment.is_rated = True
        appointment.save()
        return HttpResponseRedirect(reverse_lazy(self.get_success_url()))

    def get_context_data(self, **kwargs):
        ret = super(QualifyAppointmentView, self).get_context_data(**kwargs)
        ret['appointment'] = get_object_or_404(Appointment.objects.all(), code=self.kwargs.get('code'))
        return ret
