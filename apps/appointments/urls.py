__author__ = 'lucaru9'
from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^api/doctoroffices/(?P<pk>\d+)/appointments/$', RegisterAppointmentAPI.as_view()),
    url(r'^validate/$', CheckAppointmentView.as_view(), name='validate'),
    url(r'^validate/(?P<code>\w+)/qualify/$', QualifyAppointmentView.as_view(), name='qualify'),

]
