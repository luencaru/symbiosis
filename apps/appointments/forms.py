from django import forms
from .models import Appointment

__author__ = 'lucaru9'


class FormCheckAppointment(forms.Form):
    code = forms.CharField(max_length=16, required=True, label='Código de Cita')
    dni = forms.CharField(max_length=8, required=True, label='Dni')

    def clean_code(self):
        code = self.cleaned_data.get('code')
        appointment = Appointment.objects.filter(code=code)
        if appointment.exists():
            if appointment.filter(is_rated=False).exists():
                return code
            else:
                raise forms.ValidationError("La cita ya fue calificada.")
        else:
            raise forms.ValidationError("No existe el código de cita!")

    def clean_dni(self):
        code = self.cleaned_data.get('code')
        dni = self.cleaned_data.get('dni')
        if Appointment.objects.filter(code=code, patient_dni=dni).exists():
            return dni
        else:
            raise forms.ValidationError("El Dni y el código de cita no coinciden.")


class FormQualifyAppointment(forms.Form):
    worry = forms.IntegerField(min_value=1, max_value=5, required=True)
    knowledge = forms.IntegerField(min_value=1, max_value=5, required=True)
    opinion = forms.IntegerField(min_value=1, max_value=5, required=True)
