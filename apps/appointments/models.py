from enum import Enum
from django.core.validators import MaxValueValidator
from django.db import models


# Create your models here.
from ..doctors.models import DoctorOffice
from symbiosis.settings.base import HASHIDS_APPOINTMENT


class Appointment(models.Model):
    class States(Enum):
        p = "Pendiente"
        r = "Realizada"
        c = "Cancelada"

    class TypesRegister(Enum):
        rm = "Registro Manual"
        ra = "Registro Automático"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    code = models.CharField(max_length=16, verbose_name="Código", null=False, blank=False, unique=True)
    date = models.DateField(verbose_name='Fecha')
    time_start = models.TimeField(verbose_name='Inicio de cita')
    time_end = models.TimeField(verbose_name='Termino de cita')
    doctor_office = models.ForeignKey(DoctorOffice, related_name='appointments', verbose_name='Consultorio')
    state = models.CharField(max_length=1, verbose_name='Estado', default=States.p.name,
                             choices=[(item.name, item.value) for item in States])
    type_register = models.CharField(max_length=2, verbose_name='Tipo de registro', default=TypesRegister.ra.name,
                                     choices=[(item.name, item.value) for item in TypesRegister])
    is_valid = models.BooleanField(default=False, verbose_name='Cita Validada?')
    is_rated = models.BooleanField(default=False, verbose_name='Está calificado')
    worry = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(5)],
                                        verbose_name='Preocupación por el paciente')
    knowledge = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(5)],
                                            verbose_name='Conocimiento de Profesional')
    opinion = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(5)],
                                          verbose_name='Opinión del consultorio')
    patient_fullname = models.CharField(max_length=200, null=True, blank=True, verbose_name='NOMBRE Paciente')
    patient_dni = models.CharField(max_length=8, null=True, blank=True, verbose_name='DNI Paciente')

    class Meta:
        verbose_name_plural = "Citas"
        verbose_name = "Cita"
        ordering = ['created_at']

    def __str__(self):
        return u'{} {}'.format(self.doctor_office.name, self.date)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Appointment, self).save()
        if self.pk and not self.code:
            self.code = HASHIDS_APPOINTMENT.encode(self.pk)
            super(Appointment, self).save(update_fields=['code'])
