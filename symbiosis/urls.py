"""symbiosis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.static import serve
from symbiosis.settings import base

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.accounts.urls', namespace='accounts', app_name='accounts')),
    url(r'^', include('apps.doctors.urls', namespace='doctors', app_name='doctors')),
    url(r'^', include('apps.appointments.urls', namespace='appointments', app_name='appointments')),
    url(r'^docs/', include('rest_framework_docs.urls')),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': base.MEDIA_ROOT, }),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
