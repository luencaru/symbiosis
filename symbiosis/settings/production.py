from .base import *

ALLOWED_HOSTS = ['138.197.18.173']
DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'symbiosisDB',
        'USER': 'postgres',
        'PASSWORD': 'symbpost',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_URL = '/static/'
APPSECRET_PROOF = False
