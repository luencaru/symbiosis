# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.colors import green

env.user = 'ubuntu'
env.host_string = '192.81.209.21'
env.password = '729351967'
home_path = "/home/ubuntu"
settings = "--settings='symbiosis.settings.production'"
activate_env = "source {}/symbiosisEnv/bin/activate".format(home_path)
manage = "python manage.py"
settings_test = "--settings='symbiosis.settings.production'"


def deploy():
    print("Beginning Deploy:")
    with cd("{}/symbiosis".format(home_path)):
        run("git pull origin master")
        run("{} && pip install -r requirements.txt".format(activate_env))
        run("{} && {} collectstatic --noinput {}".format(activate_env, manage,
                                                         settings))
        run("{} && {} migrate {}".format(activate_env, manage, settings))
        sudo("service nginx restart", pty=False)
        sudo("service supervisor restart", pty=False)
    print(green("Deploy glup successful"))


def createsuperuser():
    with cd("{}/glup-backend".format(home_path)):
        run("{} && {} createsuperuser {}".format(activate_env, manage,
                                                 settings))
    print(green("Createsuperuser glup successful"))
